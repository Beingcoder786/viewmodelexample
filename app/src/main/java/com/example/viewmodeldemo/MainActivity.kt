package com.example.viewmodeldemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity(),LifecycleOwner {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var txtView:TextView=findViewById(R.id.txtView)
        var btnAdd:Button=findViewById(R.id.btnAdd)

        var viewModel=ViewModelProvider(this).get(MainActivityViewModel::class.java)
     txtView.text=viewModel.number.toString()
        btnAdd.setOnClickListener {
            viewModel.addNumber()
            txtView.text=viewModel.number.toString()
        }
    }
}